import scrapy
from getkazi.items import JobItem


class JobSpider(scrapy.Spider):
    name = "jobs"

    start_urls = [
            'https://www.brightermonday.co.ke/search/jobs-in-nairobi?page=1',
        ]

    def parse(self, response):
        # follow links to job pages
        for href in response.css(
                    'a.search-result__job-title.metrics-apply-now::attr(href)'
                ).extract():
            yield scrapy.Request(response.urljoin(href),
                                 callback=self.parse_job)

        # follow pagination links
        next_page = response.xpath('//a[@rel="next"]/@href').extract()[0]
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_job(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first().strip()

        title = extract_with_css('div.job-header__title::text')
        url = response.url
        location = extract_with_css('span.job-header__location::text')
        category = extract_with_css('span.job-header__work-type::text')
        description = extract_with_css('article.job__details.card p::text')
        return JobItem(title=title, location=location, category=category,
                       description=description, url=url)
